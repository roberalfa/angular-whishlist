import { DestinoViaje } from './destino-viaje.model';
import { Subject, BehaviorSubject } from 'rxjs';

export class DestinosApiClient {
	destinos: DestinoViaje[];
	current: Subject<DestinoViaje>  = new  BehaviorSubject<DestinoViaje>(null);
	constructor() {
       this.destinos = [];
	}
	add(d: DestinoViaje){
	  this.destinos.push(d);
	}
	getAll(){
	  return this.destinos;
	}
	elegir(d: DestinoViaje){
		this.destinos.forEach(x => x.setSelected(false));
		d.setSelected(true);
		this.current.next(d);
	}
	subscribeOnChange(fn) {
		this.current.subscribe(fn);
	}
}